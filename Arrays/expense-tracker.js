
const account = {
  name: 'Radek Jedrej',
  expenses: [],
  income: [],
  addExpense: function(description, amount) {
    this.expenses.push({
      description: description,
      amount: amount
    })
  },
  getAccountSummary: function() {
    let totalIncome = 0
    let totalExpences = 0
    let total = 0

    this.expenses.forEach(function(obj) {
      totalExpences = totalExpences + obj.amount
    })
    this.income.forEach(function(obj){
      totalIncome = totalIncome + obj.amount
    })
    total = totalIncome - totalExpences

    return `${this.name} has $${total} $${totalExpences} in expenses and $${totalIncome} in income`
  },
  addIncome: function(description, amount) {
    this.income.push({
      description: description,
      amount: amount
    })
  }
}

// Expense -> description, amount
// addExpense -> description, amount
// getAccountSummary -> total up all expenses -> Radek Jedrej has $1250 in expences. -> forEach


account.addExpense('Rent', 950)
account.addExpense('Coffe', 2)
account.addIncome('project',950)
account.addIncome('Old graphic card', 100)
console.log(account)
console.log(account.getAccountSummary())


