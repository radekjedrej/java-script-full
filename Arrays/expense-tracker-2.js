const account = {
  name: 'Radek Jedrej',
  income: [],
  expense: [],
  addExpense: function(description, amount) {
    this.expense.push({
      description: description,
      amount: amount
    })
  },
  addIncome: function(description, amount) {
    this.income.push({
      description: description,
      amount: amount
    })
  },
  accountSummary: function(){
    let totalExpense = 0
    let totalIncome = 0
    let total = 0

    this.expense.forEach(function(el) {
      totalExpense = totalExpense + el.amount
    })
    this.income.forEach(function(el) {
      totalIncome = totalIncome + el.amount
    })
    total = totalIncome - totalExpense

    return `$${this.name} exp: $${totalExpense} inc:$${totalIncome} sum: $${total}`
  }
}





account.addExpense('Bike', 560)
account.addExpense('Travel', 1560)
account.addIncome('Job', 2560)
console.log(account.accountSummary())
