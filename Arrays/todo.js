

const todos = [{
  'text': 'Washing',
  'completed': true
},{
  'text': 'Cleaning',
  'completed': false
},{
  'text': 'Hover',
  'completed': true
},{
  'text': 'Bath',
  'completed': false
},{
  'text': 'Study',
  'completed': true
}]

// Way 1
const sortTodos = function(notes) {
  notes.sort(function(a, b) {
    if(a.completed === b.completed)  {
      return 0
    } else if(a.completed) {
      return 1
    } else {
      return -1
    }
  })
}

// Way 2
const sortTodos = function(notes) {
  notes.sort(function(a, b) {
    if(!a.completed && b.completed)  {
      return -1
    } else if(!b.completed && a.completed) {
      return 1
    } else {
      return 0
    }
  })
}


sortTodos(todos)
console.log(todos)


// const getThingsToDo = function (notes) {
//   return notes.filter(function(note, index) {
//     return !note.completed
//   })
// }

// console.log(getThingsToDo(todos))

  
// const deleteTodo = function(notes, noteTitle) {
//     const index = notes.findIndex(function(note, index) {
//       return note.text.toLowerCase() === noteTitle.toLowerCase()
//     })

//     if(index > -1){
//        notes.splice(index,1)
//     }
// }


// const result = deleteTodo(todos,'washing')
// console.log(result)
// console.log(todos)

