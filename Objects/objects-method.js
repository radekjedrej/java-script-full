

let restaurant = {
  name: 'Kebab',
  guestCapacity: 75,
  guestCount: 0,
  checkAvailability: function (partySize) {
    let seatsLeft = this.guestCapacity - this.guestCount
    return partySize <= seatsLeft
  },
  seatParty: function(partySize) {
    this.guestCount += partySize
  },
  removeParty: function(partySize) {
    this.guestCount -= partySize
  }
}

// seatParty
// removeParty


restaurant.seatParty(73)
console.log(restaurant.checkAvailability(5))
restaurant.removeParty(10)
console.log(restaurant.checkAvailability(5))
