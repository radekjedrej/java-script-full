

let name = '  Radek Jedrej  '

// Length property
console.log(name.length)

// Convert to upper case
console.log(name.toUpperCase())

// Convert to lower case
console.log(name.toLowerCase())

// Include method
let password = 'abc123passdddword'
console.log(password.includes('password'))

// Trim
console.log(name.trim())



let isValidPassword = function(password) {
  let passwordTrim = password.trim()

  if(passwordTrim.length > 8 && !passwordTrim.includes('password') ) {
    return true
  } else {
    return false
  }
}

console.log(isValidPassword('asbac'))
console.log(isValidPassword('password'))
console.log(isValidPassword('doktrynasieeeee'))