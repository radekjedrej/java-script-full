

let myAccount = {
  name: 'Radek Jedrej',
  expences : 0,
  income: 0
}


let addExpense = function (account, amount) {
  account.expences = account.expences + amount
}

let addIncome = function(account, amount) {
  account.income = account.income + amount
}

let resetAccount = function(account) {
  account.expences = 0,
  account.income = 0
}

let accountSummary = function(account) {
  let total = account.income - account.expences
  return (`Account for ${account.name} has $${total} balance. $${account.income} income, and $${account.expences} expences`)
}


addExpense(myAccount, 2.50)
addIncome(myAccount, 100)
addExpense(myAccount, 30)
console.log(myAccount)
console.log(accountSummary(myAccount))
resetAccount(myAccount)
console.log(accountSummary(myAccount))

// resetAccount(myAccount)
