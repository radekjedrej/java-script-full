




let person = {
  name: 'Mark',
  age: 32,
  location: 'Kwidzyn'
}



console.log(`${person.name} is ${person.age} and lives in ${person.location}`)

person.age = person.age + 1

console.log(`${person.name} is ${person.age} and lives in ${person.location}`)

person.age += 1

console.log(`${person.name} is ${person.age} and lives in ${person.location}`)