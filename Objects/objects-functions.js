let myBook = {
  title: '1984',
  author: 'George Orwell',
  pageCount: 326
}

let otherBook = {
  title: 'A Peoples History',
  author: 'Horward Zinn',
  pageCount: 723
}


let getSummary = function(book){
  return {
    summary: `${book.title} by ${book.author}`,
    pageCountSummary: `${book.title} is ${book.pageCount} pages long`
  }
}

let bookSummary = getSummary(myBook)
let otherBookSummary = getSummary(otherBook)

console.log(bookSummary.pageCountSummary)


let allTemperatures = function(data) {
  let celcius = (data - 32) * 5/9
  let kelvin = (data + 459.67) * 5/9

  return {
    celcius: `${celcius}`,
    kelvin: `${kelvin}`,
    farenheit: `${data}`
  }
}


let converter = allTemperatures(32)
console.log(converter.celcius)


let tempCalc = function(data) {
  return {
    celcius: (data - 32) * 5/9,
    kelvin: (data + 459.67) * 5/9,
    farenheit: data
  }
}


let converter2 = tempCalc(32)
console.log(converter2)