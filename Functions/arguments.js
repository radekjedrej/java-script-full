

// Multiple Arguments
let add = function (a, b, c) {
  return a + b + c
}

let result = add(10, 1, 5)
console.log(result)

// Default Arguments
let getScoreText = function (name = 'Player', score = 0) {
  return 'Name: ' + name + ' - Score: ' + score
}

let scoreText = getScoreText('Micheal', 20)
console.log(scoreText)

let tipCalculator = function (cash, percent = 10) {
  let totalTip = cash * (percent / 100)
  return `A ${percent}% tip on $${cash} would be ${totalTip}`
}


let myFirstBill = tipCalculator(100, 20)
let myFirstBill2 = tipCalculator(36, 10)
let myFirstBill3 = tipCalculator(36)
console.log(myFirstBill)
console.log(myFirstBill2)
console.log(myFirstBill3)


