

const scoreCalculator = function(score, total){
  const finalScore = (score / total) * 100
  let grade = ''

      if(finalScore >= 90 ){
      grade = 'A'
    } else if (finalScore >= 80) {
      grade = 'B'
    } else if (finalScore >= 70) {
      grade = 'C'
    } else if (finalScore >= 60) {
      grade = 'D'
    } else {
      grade = 'F'
    }

    return `Your score was - ${score} out of ${total}. That gave us ${finalScore}% and you get grade ${grade} `
}


const first = scoreCalculator(2, 20)
console.log(first)


// if(finalScore >= 90 && finalScore <= 100){
//   grade = 'A'
//   return `Your score was - ${score} out of ${total}. That gave us ${finalScore} and you get grade ${grade} `
// } else if (finalScore >= 80 && finalScore < 89) {
//   grade = 'B'
// } else if (finalScore >= 70 && finalScore < 79) {
//   grade = 'C'
// } else if (finalScore >= 60 && finalScore < 69) {
//   grade = 'D'
// } else {
//   grade = 'F'
// }