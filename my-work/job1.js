const notes = [{
  title: 'My next trip',
  body: 'I would like to go to Spain'
},{
  title: 'Habbits to work on',
  body: 'Excercise. Eating a bit better'
},{
  title: 'Office modification',
  body: 'Get a new seat'
}]


const filters = {
  searchText : ''
}



const renderNotes = function (notes, filters) {
  const filteredNote = notes.filter(function (note) {
    return note.title.toLowerCase().includes(filters.searchText.toLowerCase())
  })

  document.querySelector('#myDiv').innerHTML = ''

  filteredNote.forEach(function (note) {
    const noteEl = document.createElement('p')
    noteEl.textContent = note.title
    document.querySelector('#myDiv').appendChild(noteEl)
  })
}

renderNotes(notes, filters)

document.querySelector('#myInput').addEventListener('input', function (e) {
  filters.searchText = e.target.value
  renderNotes(notes, filters)
})