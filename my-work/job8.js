const todos = [{
  text: 'Washing',
  completed: true
},{
  text: 'Cleaning',
  completed: false
},{
  text: 'Hover',
  completed: true
},{
  text: 'Bath',
  completed: false
},{
  text: 'Study',
  completed: true
}]


const filter = {
  tag: ''
}

const searchTodos = function (todos, filter) {
  const eachFilterTodo = todos.filter( function(todo) {
    return todo.text.toLowerCase().includes(filter.tag.toLowerCase())
  })
  document.querySelector('.myDiv').innerHTML = ''

  const notDoneTodo = eachFilterTodo.filter(function (todo){
    return !todo.completed
  })

  const titleWrapper = document.createElement('h1')
  titleWrapper.textContent = `You have ${notDoneTodo.length} todos left`
  document.querySelector('.myDiv').appendChild(titleWrapper)

  eachFilterTodo.forEach(function (el) {
    const elWrapper = document.createElement('p')
    elWrapper.textContent = el.text
    document.querySelector('.myDiv').appendChild(elWrapper)
  })
}

searchTodos(todos, filter)

document.querySelector('#filterList').addEventListener('input', function(e) {

  filter.tag = e.target.value
  searchTodos(todos, filter)

})

document.querySelector('#myForm').addEventListener('submit', function(e) {
  e.preventDefault()

  todos.push({
    text: e.target.elements.submitButton.value,
    completed: false
  })

  e.target.elements.submitButton.value = ''

  searchTodos(todos, filter)
})