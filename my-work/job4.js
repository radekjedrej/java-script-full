const todos = [{
  text: 'Washing',
  completed: true
},{
  text: 'Cleaning',
  completed: false
},{
  text: 'Hover',
  completed: true
},{
  text: 'Bath',
  completed: false
},{
  text: 'Study',
  completed: true
}]


mySearch = {
  key1: ''
}

const displayText = function (todos, key) {
  const results = todos.filter(function (todo) {
    return todo.text.toLowerCase().includes(key.key1.toLowerCase())
  })

  const incompletedTodos = results.filter( function (todo) {
    return !todo.completed
  })

  document.querySelector('#myDiv').innerHTML = ''

  const summary = document.createElement('h2')
  summary.textContent = `You have ${incompletedTodos.length} todos left`
  document.querySelector('#myDiv').appendChild(summary)
  

  results.forEach(function (e) {
    const contentDiv = document.createElement('p')
    contentDiv.textContent = e.text
    document.querySelector('#myDiv').appendChild(contentDiv)

  })
}

document.querySelector('#myInput').addEventListener('input', function (e) {
  mySearch.key1 = e.target.value
  displayText(todos, mySearch)
  console.log(incompletedTodos.length)
})




