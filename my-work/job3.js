const notes = [{
  title: 'My next trip',
  body: 'I would like to go to Spain'
},{
  title: 'Habbits to work on',
  body: 'Excercise. Eating a bit better'
},{
  title: 'Office modification',
  body: 'Get a new seat'
}]



const filters = {
  myFilter: ''
}

const displayObject = function (notes, filters) {
  const notesEl = notes.filter(function (note) {
    return note.title.toLowerCase().includes(filters.myFilter.toLowerCase())
  })

  document.querySelector('#myDiv').innerHTML = ''

  notesEl.forEach(function (note) {
    const elDiv = document.createElement('p')
    elDiv.textContent = note.title
    document.querySelector('#myDiv').appendChild(elDiv)
  })

}

displayObject(notes, filters)

document.querySelector('#myInput').addEventListener('input', function(e) {
  filters.myFilter = e.target.value
  displayObject(notes, filters)
})