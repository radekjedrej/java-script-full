const todos = [{
  text: 'Washing',
  completed: true
},{
  text: 'Cleaning',
  completed: false
},{
  text: 'Hover',
  completed: true
},{
  text: 'Bath',
  completed: false
},{
  text: 'Study',
  completed: true
}]

const filter = {
  tag: ''
}

const filterTodos = function (todos, filter) {
  const results = todos.filter(function(todo) {
    return todo.text.toLowerCase().includes(filter.tag.toLowerCase())
  })

  document.querySelector('.myDiv').innerHTML = ''

  const todosCompleted = todos.filter(function(todo) {
    return !todo.completed
  })

  const title = document.createElement('h2')
  title.textContent = `You have ${todosCompleted.length} todo`
  document.querySelector('.myDiv').appendChild(title)


  results.forEach(function (note) {
  const block = document.createElement('p')
  block.textContent = note.text
  document.querySelector('.myDiv').appendChild(block)
  })
}

filterTodos(todos, filter)

document.querySelector('#filterList').addEventListener('input', function (e) {
  filter.tag = e.target.value
  filterTodos(todos, filter)
})


document.querySelector('#myForm').addEventListener('submit', function(e){
  e.preventDefault()

  todos.push({
    text: e.target.elements.submitButton.value,
    completed: false
  })

  e.target.elements.submitButton.value = ''

  filterTodos(todos, filter)



})

