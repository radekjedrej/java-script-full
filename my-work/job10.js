const todos = [{
  text: 'Washing',
  completed: true
},{
  text: 'Cleaning',
  completed: false
},{
  text: 'Hover',
  completed: true
},{
  text: 'Bath',
  completed: false
},{
  text: 'Study',
  completed: true
}]


const filter = {
  match: '',
  completed: false
}


const searchWords = function (todos, filters) {
  const resultWords = todos.filter(function (note) {
    const textFilterResults = note.text.toLowerCase().includes(filters.match.toLowerCase())
    const completedFilterResults = !filter.completed || note.completed

    return textFilterResults && completedFilterResults
  })

  document.querySelector('.myDiv').innerHTML = ''

  const todosLeft = resultWords.filter(function (el) {
    return !el.completed
  })

  const howManyLeft = document.createElement('h2')
  howManyLeft.textContent = `You have left ${todosLeft.length} todos`
  document.querySelector('.myDiv').appendChild(howManyLeft)

  resultWords.forEach(function (el) {
    const elDiv = document.createElement('p')
    elDiv.textContent = el.text
    document.querySelector('.myDiv').appendChild(elDiv)
  })
}

searchWords(todos, filter)

document.querySelector('#filterList').addEventListener('input', function(e) {
  filter.match = e.target.value
  searchWords(todos, filter)
})

document.querySelector('#myForm').addEventListener('submit', function(e) {
  e.preventDefault()

  todos.push({
    text: e.target.elements.submitButton.value,
    completed: false
  })

  e.target.elements.submitButton.value = ''
  searchWords(todos, filter)

})

document.querySelector('#hide-completed').addEventListener('change', function(e) {
  filter.completed = e.target.checked
  searchWords(todos, filter)
})
